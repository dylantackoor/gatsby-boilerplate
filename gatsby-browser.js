/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

// TODO: enable this when below PR is merged
// https://github.com/gatsbyjs/gatsby/pull/10432
// Reload page with new
// exports.onServiceWorkerUpdateReady = () => window.location.reload(true)
