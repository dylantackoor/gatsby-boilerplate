module.exports = {
    extends: `airbnb-base`,
    env: {
        node: true,
        es6: true
    },
    rules: {
        semi: [`error`, `never`],
        quotes: [`error`, `backtick`, `avoid-escape`],
        indent: [`error`, `tab`],
        'no-tabs': 0,
        'quote-props': [`error`, `as-needed`],
        'comma-dangle': [`error`, `only-multiline`],
    }
};
