/* eslint-disable no-console */
const dotenv = require(`dotenv`)

const {
	ACTIVE_ENV,
	NODE_ENV,
	BRANCH,
} = process.env

// Default to local development env
let dotEnvPath = `.env`

// Check supplied environment
if (!ACTIVE_ENV && NODE_ENV === `production`) {
	dotEnvPath += `.development`
} else if (BRANCH === `staging`) {
	dotEnvPath += `.staging`
} else if (ACTIVE_ENV === `production`) {
	dotEnvPath += `.production`
}

// Load config
console.log(`Using environment config: '${dotEnvPath}'`)
const result = dotenv.config({ path: dotEnvPath })
if (result.error) {
	throw result.error
} else {
	console.log(result.parsed)
}

// Determine Netlify headers
const netlifyHeaders = { '/sw.js': [`Cache-Control: no-cache`] }
if (ACTIVE_ENV === `staging`) {
	netlifyHeaders[`/*`] = [
		`Basic-Auth: dylantackoor:crimson guest:crimson`,
	]
}

const {
	DEPLOYMENT_URL,
	SOURCE_DOMAIN,
	SOURCE_PROTOCAL,
	WP_USER,
	WP_PASS,
	// GA_TRACKING_ID,
	FAVICON_PATH,
} = process.env

const gatsbyConfig = {
	siteMetadata: {
		title: `Dylan Tackoor`,
		description: `built by Dylan.`,
		author: `@dylantackoor`,
		siteUrl: DEPLOYMENT_URL,
	},
	plugins: [
		`gatsby-plugin-typescript`,
		`gatsby-plugin-tslint`,
		`gatsby-plugin-styled-components`,
		`gatsby-plugin-react-helmet`, // Sets elements in <head>
		`gatsby-plugin-zopfli`,
		`gatsby-plugin-brotli`,
		{
			resolve: `gatsby-source-wordpress`,
			options: {
				baseUrl: SOURCE_DOMAIN, // without protocol
				protocol: SOURCE_PROTOCAL,
				hostingWPCOM: false,
				useACF: true,
				verboseOutput: true,
				auth: {
					htaccess_user: WP_USER,
					htaccess_pass: WP_PASS,
				},
				searchAndReplaceContentUrls: {
					sourceUrl: `${SOURCE_PROTOCAL}://${SOURCE_DOMAIN}`,
					replacementUrl: DEPLOYMENT_URL,
				},
				excludedRoutes: [
					`**/wpcom/***`,
					`**/yoast/***`,
					`**/jetpack/**`
				]
			},
		},
		{
			resolve: `gatsby-plugin-netlify`,
			options: {
				headers: netlifyHeaders,
				allPageHeaders: [],
				mergeSecurityHeaders: true,
				mergeLinkHeaders: true,
				mergeCachingHeaders: true,
				transformHeaders: headers => headers,
				generateMatchPathRewrites: true,
			},
		},
		{
			resolve: `gatsby-plugin-web-font-loader`,
			options: {
				google: {
					families: [`Dosis`]
				}
			}
		},
		{
			resolve: `gatsby-plugin-google-tagmanager`,
			options: {
				id: `GTM-MQSFT75`,
				includeInDevelopment: false,
			},
		},
		{
			resolve: `gatsby-plugin-canonical-urls`,
			options: {
				siteUrl: DEPLOYMENT_URL,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`,
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		`gatsby-plugin-sitemap`,
		`gatsby-plugin-netlify-cache`,
		{
			resolve: `gatsby-plugin-webpack-size`,
			options: {
				development: true,
			},
		},
		{
			resolve: `gatsby-plugin-webpack-bundle-analyzer`,
			options: {
				defaultSizes: `gzip`,
				analyzerPort: 8001,
				production: false,
			},
		},
	],
}

// Web App Manifest
gatsbyConfig.plugins.push({
	resolve: `gatsby-plugin-manifest`,
	options: {
		name: gatsbyConfig.siteMetadata.title,
		short_name: `starter`,
		start_url: `/`,
		background_color: `#663399`,
		theme_color: `#663399`,
		display: `standalone`,
		icon: FAVICON_PATH,
	},
})

if (NODE_ENV !== `production`) {
	gatsbyConfig.plugins.push({
		resolve: `gatsby-plugin-accessibilityjs`,
		options: {
			onError: (error) => {
				console.error(`error`)
				console.error(error)
			},
			injectStyles: `
        .accessibility-error {
          box-shadow: 0 0 3px 1px #f00;
          background-color: rgba(255, 0, 0, 0.25);
          position: relative;
        }
        .accessibility-error:before {
          content: "A11Y";
          position: absolute;
          top: 0;
          left: 0;
          color: #fff;
          font-size: 10px;
          background-color: rgba(255, 0, 0, 0.5);
          transform: translateY(-100%);
        }
      `,
		},
	})
}

// Add these last, specifically in this order
gatsbyConfig.plugins.push(
	// `gatsby-plugin-sri`,
	// `gatsby-plugin-offline`,
)

module.exports = gatsbyConfig
