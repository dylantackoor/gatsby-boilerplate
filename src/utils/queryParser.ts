import { FooterProps } from '../components/footer'
import { LayoutProps } from '../components/layout'
import { NavBarProps } from '../components/navbar'
import { MenuQuery, PostsQuery } from '../types/queries'

// allWordpressWpApiMenusMenusItems
export function parseMenuQuery(menuItems: MenuQuery): LayoutProps {
	let navProps: NavBarProps
	let footerProps: FooterProps

	for (const { node } of menuItems.edges) {
		const props = {
			links: node.items.map((item: any) => {
				try {
					const parsedSrc = new URL(item.url).pathname
					return {
						src: parsedSrc,
						text: item.title,
					}
				} catch (error) {
					console.error(error)
					console.error(item)
				}
			}),
		}

		// Assign
		if (node.name === 'Navbar') {
			navProps = props
		} else if (node.name === 'Footer') {
			footerProps = props
		}
	}

	return { navProps, footerProps }
}
