import { graphql } from 'gatsby'
import React, { Component } from 'react'
import { Layout } from '../components/layout'
import { MenuQuery } from '../types/queries'
import { parseMenuQuery } from '../utils/queryParser'

interface PageProps {
	data: {
		allWordpressWpApiMenusMenusItems: MenuQuery
		wordpressPage: {
			title: string
			content: string
			slug: string
			id: string
			date: any
		},
		// site: {
		// 	siteMetadata: {
		// 		title: string
		// 		subtitle: string
		// 	}
		// }
	}
}

export default class PageTemplate extends Component<PageProps> {
	render() {
		const {
			allWordpressWpApiMenusMenusItems,
			// site,
			wordpressPage,
		} = this.props.data

		// const { siteMetadata } = site
		const { navProps, footerProps } = parseMenuQuery(allWordpressWpApiMenusMenusItems)

		return (
			<Layout
				navProps={navProps}
				footerProps={footerProps}
			>
				<h1>{wordpressPage.title}</h1>
				<p>{wordpressPage.date}</p>
				<div dangerouslySetInnerHTML={{ __html: wordpressPage.content }} />
			</Layout>
		)
	}
}

export const pageQuery = graphql`
	query currentPageQuery($id: String!) {
		wordpressPage(id: { eq: $id }) {
			title
			content
			slug
			id
			date(formatString: "MMMM DD, YYYY")
		}
		allWordpressWpApiMenusMenusItems {
			edges {
				node {
					name
					description
					items {
						order
						title
						url
						attr
						object_slug
						target
						classes
					}
				}
			}
		}
	}
`

// site {
// 	id
// 	siteMetadata {
// 		title
// 	}
// }
