import { graphql } from 'gatsby'
import React, { Component } from 'react'
import { Layout } from '../components/layout'
import { MenuQuery } from '../types/queries'
import { parseMenuQuery } from '../utils/queryParser'

interface PostProps {
	data: {
		allWordpressWpApiMenusMenusItems: MenuQuery
		wordpressPost: {
			title: string
			content: string
		},
		site: {
			title: string
			subtitle: string
		}
	}
}

export default class PostTemplate extends Component<PostProps> {
	render() {
		const {
			allWordpressWpApiMenusMenusItems,
			site,
			wordpressPost,
		} = this.props.data

		const { navProps, footerProps } = parseMenuQuery(allWordpressWpApiMenusMenusItems)

		return (
			<Layout
				navProps={navProps}
				footerProps={footerProps}
			>
				<h1>{wordpressPost.title}</h1>
				<div dangerouslySetInnerHTML={{ __html: wordpressPost.content }}/>
			</Layout>
		)
	}
}

export const pageQuery = graphql`
	query currentPostQuery($id: String!) {
		wordpressPost(id: { eq: $id }) {
			title
			content
		}
		site {
			siteMetadata {
				title
			}
		}
		allWordpressWpApiMenusMenusItems {
			edges {
				node {
					name
					description
					items {
						order
						title
						url
						attr
						object_slug
						target
						classes
					}
				}
			}
		}
	}
`
