export interface MenuQuery {
	edges: Array<{
		node: {
			name: string
			slug: string
			description: string
			items: Array<{
				order: number
				title: string
				url: string
				attr: string
				object_slug: string
				target: string
				classes: string,
			}>,
		},
	}>,
}

export interface PostsQuery {
	totalCount: number
	edges: Array<{
		node: {
			id: string
			title: string
			date: string
			excerpt: string
			link: string
		}
	}>
}
