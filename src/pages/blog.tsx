import { graphql, Link } from 'gatsby'
import React, { Component } from 'react'
import styled from 'styled-components'
import { Header } from '../components/header'
import { Layout } from '../components/layout'
import { PostItems } from '../components/postList'
import { MenuQuery, PostsQuery } from '../types/queries'
import { parseMenuQuery } from '../utils/queryParser'

export interface BlogPageProps {
	data: {
		allWordpressWpApiMenusMenusItems: MenuQuery,
		allWordpressPost: PostsQuery,
	}
}

const BlogList = styled.ul`
`

export default class BlogPage extends Component<BlogPageProps> {
	render() {
		const { allWordpressWpApiMenusMenusItems, allWordpressPost } = this.props.data
		const { navProps, footerProps } = parseMenuQuery(allWordpressWpApiMenusMenusItems)
		const postList = PostItems(allWordpressPost)

		return (
			<Layout
				navProps={navProps}
				footerProps={footerProps}
			>
				{/* header com */}
				{/* bloglist */}
					{/* blotitem */}

				<h1>Blog!</h1>
				<p>Found {allWordpressPost.totalCount} posts!</p>
				<BlogList>
					{postList}
				</BlogList>
			</Layout>
		)
	}
}

export const query = graphql`
	query {
		allWordpressWpApiMenusMenusItems {
			edges {
				node {
					name
					description
					items {
						order
						title
						url
						attr
						object_slug
						target
						classes
					}
				}
			}
		}
		allWordpressPost {
			totalCount
			edges {
				node {
					id
					title
					date
					excerpt
					link
				}
			}
		}
	}
`
