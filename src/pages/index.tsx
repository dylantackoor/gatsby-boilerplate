import { graphql } from 'gatsby'
import React, { Component } from 'react'
import styled from 'styled-components'
import { Header } from '../components/header'
import { Layout } from '../components/layout'
import { MenuQuery } from '../types/queries'
import { parseMenuQuery } from '../utils/queryParser'

export interface IndexPageProps {
	data: {
		allWordpressWpApiMenusMenusItems: MenuQuery,
	}
}

export default class IndexPage extends Component<IndexPageProps> {
	render() {
		const { allWordpressWpApiMenusMenusItems } = this.props.data
		const { navProps, footerProps } = parseMenuQuery(allWordpressWpApiMenusMenusItems)

		return (
			<Layout
				navProps={navProps}
				footerProps={footerProps}
			>
				<Header />
				<h1>Prod + staging work!</h1>
			</Layout>
		)
	}
}

export const query = graphql`
	query {
		allWordpressWpApiMenusMenusItems {
			edges {
				node {
					name
					description
					items {
						order
						title
						url
						attr
						object_slug
						target
						classes
					}
				}
			}
		}
	}
`
