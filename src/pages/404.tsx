import { graphql } from 'gatsby'
import React, { Component } from 'react'
import { Layout } from '../components/layout'
import { MenuQuery } from '../types/queries'
import { parseMenuQuery } from '../utils/queryParser'

export interface PageNotFoundProps {
	data: {
		allWordpressWpApiMenusMenusItems: MenuQuery,
	}
}

export default class PageNotFound extends Component<PageNotFoundProps> {
	render() {
		const { allWordpressWpApiMenusMenusItems } = this.props.data
		const { navProps, footerProps } = parseMenuQuery(allWordpressWpApiMenusMenusItems)

		return (
			<Layout
				navProps={navProps}
				footerProps={footerProps}
			>
				<h1>NOT FOUND</h1>
				<p>You just hit a route that doesn&#39;t exist... the sadness.</p>
			</Layout>
		)
	}
}

export const query = graphql`
	query {
		allWordpressWpApiMenusMenusItems {
			edges {
				node {
					name
					description
					items {
						order
						title
						url
						attr
						object_slug
						target
						classes
					}
				}
			}
		}
	}
`
