import { Link } from 'gatsby'
import React, { Component } from 'react'
import styled from 'styled-components'

// Data
export interface NavLinkProps {
	src: string
	text: string
}

export interface NavBarProps {
	// logo: {
	// 	src: string
	// 	alt: string,
	// },
	links: NavLinkProps[]
}

// Elements
const NavBarContainer = styled('nav')`
`

const NavLogo = styled('img')`
`

const NavLinksContainer = styled(`ul`)`
`

const NavLink = styled(Link)`
`

// Component
export class NavBar extends Component<NavBarProps> {
	public render() {
		const links = this.props.links.map(link => <li key={link.src}><NavLink to={link.src}>{link.text}</NavLink></li>)

		return (
			<NavBarContainer>
				{/* <NavLogo src={this.props.logo.src} alt={this.props.logo.alt} /> */}
				<NavLinksContainer>{links}</NavLinksContainer>
			</NavBarContainer>
		)
	}
}
