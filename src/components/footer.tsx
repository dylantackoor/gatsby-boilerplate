import { Link } from 'gatsby'
import React, { Component } from 'react'
import styled from 'styled-components'

// Data
export interface FooterLinkProps {
	src: string
	text: string
}

export interface FooterProps {
	// logo: {
	// 	alt: string,
	// 	src: string,
	// },
	links: FooterLinkProps[]
}

// Elements
const FooterContainer = styled('footer')`
`

const FooterLogo = styled('img')`
`

const FooterLinksContainer = styled(`ul`)`
`

const FooterLink = styled(Link)`
`

// Component
export class Footer extends Component<FooterProps> {
	public render() {
		const links = this.props.links.map(link => <FooterLink key={link.src} to={link.src}>{link.text}</FooterLink>)

		return (
			<FooterContainer>
				{/* <FooterLogo src={this.props.logo.src} alt={this.props.logo.alt} /> */}
				<FooterLinksContainer>{links}</FooterLinksContainer>
			</FooterContainer>
		)
	}
}
