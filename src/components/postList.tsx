import { Link } from 'gatsby'
import React from 'react'
import { PostsQuery } from '../types/queries'

export const PostItems = (allWordpressPost: PostsQuery) => {
	const list = allWordpressPost.edges.map(results => {
		const post = results.node
		const localLink = new URL(post.link).pathname
		return (
			<li key={post.id}><Link to={localLink}>{post.title}</Link></li>
		)
	})

	return (<ul>{list}</ul>)
}
