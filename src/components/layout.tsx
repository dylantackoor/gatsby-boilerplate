import React, { Component } from 'react'
import { createGlobalStyle } from 'styled-components'
import { Footer, FooterProps } from './footer'
import { NavBar, NavBarProps } from './navbar'

const GlobalStyle = createGlobalStyle`
  p,
	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
    font-family: 'Dosis', sans-serif;
  }
`

export interface LayoutProps {
	navProps: NavBarProps
	footerProps: FooterProps
}

export class Layout extends Component<LayoutProps> {
	public render() {
		return (
			<div>
				<GlobalStyle />

				<NavBar
					// logo={this.props.navProps.logo}
					links={this.props.navProps.links}
				/>

				{this.props.children}

				<Footer
					// logo={this.props.footerProps.logo}
					links={this.props.footerProps.links}
				/>
			</div>
		)
	}
}
