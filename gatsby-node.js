/* eslint-disable no-console */

const path = require(`path`)
const slash = require(`slash`)

// Allow local SSL in development
if (!process.env.ACTIVE_ENV) {
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = `0`
}

exports.createPages = ({ graphql, actions }) => {
	// Expose Gatsby APIs
	const {
		createPage,
		createRedirect,
	} = actions

	// Create Redirects
	console.log(`Creating redirect /wp-admin to https://wp.dylantackoor.com/wp-admin`)
	createRedirect({ fromPath: `/wp-admin`, toPath: `https://wp.dylantackoor.com/wp-admin`, isPermanent: true })

	// Scrape WordPress
	const pageTemplate = path.resolve(`./src/templates/page.tsx`)
	const postTemplate = path.resolve(`./src/templates/post.tsx`)

	const pageQuery = graphql(`{
		allWordpressPage {
			edges {
				node {
					id
					link
					status
					template
				}
			}
		}
	}`).then((pageResults) => {
		if (pageResults.errors) {
			Promise.reject(pageResults.errors)
		}

		pageResults.data.allWordpressPage.edges.forEach((edge) => {
			const newPagePath = new URL(edge.node.link).pathname
			console.log(`Creating page ${newPagePath}`)
			createPage({
				path: newPagePath,
				component: slash(pageTemplate),
				context: {
					id: edge.node.id,
				},
			})
		})
	})

	const postQuery = graphql(`{
		allWordpressPost {
			edges {
				node {
					id
					link
					status
					template
					format
				}
			}
		}
	}`).then((postResults) => {
		postResults.data.allWordpressPost.edges.forEach((edge) => {
			const newPagePath = new URL(edge.node.link).pathname
			console.log(`Creating post ${newPagePath}`)
			createPage({
				path: newPagePath,
				component: slash(postTemplate),
				context: {
					id: edge.node.id,
				},
			})
		})
	})

	return Promise.all([pageQuery, postQuery])
}
